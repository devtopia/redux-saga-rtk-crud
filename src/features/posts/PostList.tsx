import { Grid } from "@mui/material";
import React from "react";

import PostCardView from "./PostCardView";
import { Post } from "./types";

interface PostListProps {
  posts: Post[];
  onDeletePost: (post: Post) => void;
  onUpdatePost: (post: Post) => void;
}

const PostList: React.FC<PostListProps> = (props) => {
  const { posts, onDeletePost, onUpdatePost } = props;
  return (
    <>
      <Grid container>
        {posts &&
          posts.map((post) => (
            <PostCardView
              key={post.id}
              post={post}
              onDeleteClick={onDeletePost}
              onUpdateClick={onUpdatePost}
            />
          ))}
      </Grid>
    </>
  );
};

export default PostList;
