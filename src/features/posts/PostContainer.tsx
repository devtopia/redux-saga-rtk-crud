import { Container } from "@mui/material";
import { useEffect } from "react";
import React from "react";

import usePostService from "../../hooks/usePostService";
import PostForm from "./PostForm";
import PostList from "./PostList";

const PostContainer = () => {
  const { posts, deletePost, updatePost, createPost, fetchAllPosts } =
    usePostService();

  useEffect(() => {
    fetchAllPosts();
  }, [fetchAllPosts]);

  return (
    <>
      <Container maxWidth="xs">
        <PostForm onSubmitClick={createPost} />
      </Container>
      <Container sx={{ py: 4 }} maxWidth="md">
        <PostList
          posts={posts}
          onDeletePost={deletePost}
          onUpdatePost={updatePost}
        />
      </Container>
    </>
  );
};

export default PostContainer;
