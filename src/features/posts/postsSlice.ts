import {
  PayloadAction,
  createAction,
  createSlice,
  nanoid,
} from "@reduxjs/toolkit";

import { RootState } from "../../app/store";
import { Post } from "./types";

export interface PostsState {
  posts: Post[];
}

const initialState: PostsState = {
  posts: [],
};

// slice
export const postsSlice = createSlice({
  name: "posts",
  initialState,
  reducers: {
    fetchAllSucceeded(state, action: PayloadAction<Post[]>) {
      state.posts = action.payload;
    },
  },
});

// actions
export const postsActions = {
  create: createAction(`${postsSlice.name}/create`, (post: Post) => ({
    payload: {
      id: nanoid(),
      title: post.title,
      body: post.body,
    },
  })),
  fetchAll: createAction(`${postsSlice.name}/fetchAll`),
  fetchAllSucceeded: postsSlice.actions.fetchAllSucceeded,
  update: createAction<Post>(`${postsSlice.name}/update`),
  delete: createAction<Post>(`${postsSlice.name}/delete`),
};

// selectors
export const selectPosts = (state: RootState) => state.posts.posts;

// reducer
export default postsSlice.reducer;
