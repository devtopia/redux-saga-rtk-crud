import { yupResolver } from "@hookform/resolvers/yup/dist/yup";
import { Button, Stack, TextField } from "@mui/material";
import React from "react";
import { Controller, useForm } from "react-hook-form";
import { useTranslation } from "react-i18next";
import * as Yup from "yup";

import { PostFormInput } from "./types";

interface PostFormProps {
  defaultValues?: PostFormInput;
  onSubmitClick(data: PostFormInput): void;
}

const PostForm = (props: PostFormProps) => {
  const { t } = useTranslation();
  const {
    defaultValues = {
      title: "",
      body: "",
    },
    onSubmitClick,
  } = props;
  const newPostValidationSchema = Yup.object().shape({
    title: Yup.string()
      .required(t("home.form.validation.title-required"))
      .max(20, t("home.form.validation.title-max", { num: 20 })),
    body: Yup.string().required(t("home.form.validation.body-required")),
  });
  const methods = useForm({
    defaultValues,
    resolver: yupResolver(newPostValidationSchema),
  });
  const { handleSubmit, reset, control } = methods;
  return (
    <Stack
      sx={{ pt: 0 }}
      direction="column"
      spacing={1}
      justifyContent="center"
    >
      <Controller
        name="title"
        control={control}
        render={({ field: { onChange, value }, fieldState: { error } }) => (
          <TextField
            helperText={error ? error.message : null}
            size="small"
            error={!!error}
            onChange={onChange}
            value={value}
            fullWidth
            label={t("hoem.form.title")}
            variant="outlined"
          />
        )}
      />
      <Controller
        name="body"
        control={control}
        render={({ field: { onChange, value }, fieldState: { error } }) => (
          <TextField
            helperText={error ? error.message : null}
            size="small"
            error={!!error}
            onChange={onChange}
            value={value}
            fullWidth
            label={t("hoem.form.body")}
            variant="outlined"
          />
        )}
      />
      <Button onClick={handleSubmit(onSubmitClick)} variant={"contained"}>
        {t("home.buttons.submit")}
      </Button>
      <Button onClick={() => reset()} variant={"outlined"}>
        {t("home.buttons.reset")}
      </Button>
    </Stack>
  );
};

export default PostForm;
