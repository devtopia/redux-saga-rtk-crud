export interface Post {
  id?: string;
  title: string;
  body: string;
}

export interface PostFormInput {
  title: string;
  body: string;
}
