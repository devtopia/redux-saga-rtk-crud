import axios from "axios";

import { Post } from "./types";

axios.defaults.baseURL = "http://localhost:8000";
const POSTS_BASE_URL = "/posts";

export const getPosts = () => axios.get<Post[]>(POSTS_BASE_URL);
export const createPost = (post: Post) => axios.post(POSTS_BASE_URL, post);
export const updatePost = (post: Post) =>
  axios.put(`${POSTS_BASE_URL}/${post.id}`, post);
export const deletePost = (post: Post) =>
  axios.delete(`${POSTS_BASE_URL}/${post.id}`, { data: post });
