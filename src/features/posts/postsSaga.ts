import { AxiosResponse } from "axios";
import { call, put, takeEvery } from "redux-saga/effects";

import { createPost, deletePost, getPosts, updatePost } from "./postsAPI";
import { postsActions } from "./postsSlice";
import { Post } from "./types";

// worker sagas
export function* onGetPosts() {
  const response: AxiosResponse<Post[]> = yield call(getPosts);
  // const posts: Post[] = yield call(getPosts);
  yield put(postsActions.fetchAllSucceeded(response.data));
}

function* onCreatePost({
  payload,
}: {
  type: typeof postsActions.create;
  payload: Post;
}) {
  yield call(createPost, payload);
  yield put(postsActions.fetchAll());
}

function* onUpdatePost({
  payload,
}: {
  type: typeof postsActions.update;
  payload: Post;
}) {
  yield call(updatePost, payload);
  yield put(postsActions.fetchAll());
}

function* onDeletePost({
  payload,
}: {
  type: typeof postsActions.delete;
  payload: Post;
}) {
  yield call(deletePost, payload);
  yield put(postsActions.fetchAll());
}

// watcher saga
export function* postsWatcherSaga() {
  yield takeEvery(postsActions.fetchAll.type, onGetPosts);
  yield takeEvery(postsActions.create.type, onCreatePost);
  yield takeEvery(postsActions.update.type, onUpdatePost);
  yield takeEvery(postsActions.delete.type, onDeletePost);
}

export default postsWatcherSaga;
