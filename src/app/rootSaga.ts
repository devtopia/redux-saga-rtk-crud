import { all, fork } from "redux-saga/effects";

import postsWatcherSaga from "../features/posts/postsSaga";

export function* rootSaga() {
  yield all([fork(postsWatcherSaga)]);
}

export default rootSaga;
