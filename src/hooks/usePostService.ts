import { useCallback } from "react";

import { useAppDispatch, useAppSelector } from "../app/hooks";
import { postsActions, selectPosts } from "../features/posts/postsSlice";
import { Post, PostFormInput } from "../features/posts/types";

const usePostService = () => {
  const dispatch = useAppDispatch();

  return {
    posts: useAppSelector(selectPosts),
    createPost: useCallback(
      (post: PostFormInput) => {
        dispatch(postsActions.create({ title: post.title, body: post.body }));
      },
      [dispatch]
    ),
    fetchAllPosts: useCallback(() => {
      dispatch(postsActions.fetchAll());
    }, [dispatch]),
    updatePost: useCallback(
      (post: Post) => {
        dispatch(
          postsActions.update({
            ...post,
            body: `Updated at ${new Date().toISOString()}`,
          })
        );
      },
      [dispatch]
    ),
    deletePost: useCallback(
      (post: Post) => {
        dispatch(postsActions.delete(post));
      },
      [dispatch]
    ),
  };
};

export default usePostService;
